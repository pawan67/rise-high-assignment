import Image from "next/image";
import { Inter } from "next/font/google";
import { useEffect, useState } from "react";
import { useRouter } from "next/router";

const inter = Inter({ subsets: ["latin"] });

export default function Home() {
  const [products, setProducts] = useState([]);
  const [page, setPage] = useState(1);
  const [totalPages, setTotalPages] = useState(0);
  const [category, setCategory] = useState("");
  const [categories, setCategories] = useState([]);
  const [productsToDisplay, setProductsToDisplay] = useState(10);
  const router = useRouter();
  const fetchProducts = async () => {
    const res = await fetch(
      `https://dummyjson.com/products${category}?limit=${productsToDisplay}&skip=${
        page * productsToDisplay - productsToDisplay
      }`
    );
    const catRes = await fetch("https://dummyjson.com/products/categories");
    const catData = await catRes.json();
    setCategories(catData);
    const data = await res.json();
    console.log(categories);
    setProducts(data.products);
    setTotalPages(data.total);
  };

  useEffect(() => {
    fetchProducts();
  }, [page, productsToDisplay, category]);

  const selectPageHandler = (selectedPage) => {
    if (
      selectedPage >= 1 &&
      selectedPage <= Math.ceil(totalPages) / productsToDisplay &&
      selectedPage !== page
    ) {
      setPage(selectedPage);
    }
  };

  return (
    <main className=" max-w-screen-xl p-10 mx-auto">
      <div className=" flex justify-between items-center my-10">
        <div>
          Products to show per page:
          <select
            defaultValue={productsToDisplay}
            name=""
            id=""
            onChange={(e) => setProductsToDisplay(e.target.value)}
          >
            <option value="2">2</option>
            <option value="5">5</option>
            <option value="10">10</option>
          </select>
        </div>
        <select
          onChange={(e) => setCategory(e.target.value)}
          defaultValue={category}
          name=""
          id=""
        >
          <option value="">All</option>
          {categories.map((category, i) => {
            return (
              <option key={i} value={`/category/${category}`}>
                {category}
              </option>
            );
          })}
        </select>
      </div>
      <div>Total products found: {totalPages}</div>

      <div className=" grid grid-cols-1 gap-5">
        {products &&
          products.length > 0 &&
          products.map((product) => {
            return (
              <div
                onClick={() => router.push(`/product/${product.id}`)}
                className=" text-gray-100 md:flex md:space-x-5 rounded-xl p-5 bg-[#181818]"
                key={product.id}
              >
                <img
                  className=" rounded-xl md:w-40 "
                  src={product.images[0]}
                  alt={product.name}
                />
                <div>
                  <h2 className=" text-3xl font-semibold">{product.title}</h2>
                  <p>{product.description}</p>
                  <button className=" bg-gray-200 text-[#181818] p-1 px-2 mt-3  rounded ">
                    view product
                  </button>
                </div>
              </div>
            );
          })}
      </div>

      {totalPages && totalPages > 0 && (
        <Pagination
          page={page}
          totalPages={totalPages}
          selectPageHandler={selectPageHandler}
          productsToDisplay={productsToDisplay}
        />
      )}
    </main>
  );
}

const Pagination = ({
  page,
  totalPages,
  selectPageHandler,
  productsToDisplay,
}) => {
  return (
    <div className=" flex gap-2 flex-wrap justify-center mt-5">
      <button
        className=" bg-gray-200 text-[#181818] p-1 px-2 mt-3  rounded "
        onClick={() => selectPageHandler(page - 1)}
      >
        prev
      </button>
      {[...Array(Math.ceil(totalPages / productsToDisplay))].map((_, i) => (
        <button
          className={` ${
            page === i + 1
              ? " bg-gray-400 text-[#181818]"
              : "bg-gray-200 text-[#181818]"
          }  p-1 px-2 mt-3  rounded `}
          onClick={() => selectPageHandler(i + 1)}
          key={i}
        >
          {i + 1}
        </button>
      ))}
      <button
        className=" bg-gray-200 text-[#181818] p-1 px-2 mt-3  rounded "
        onClick={() => selectPageHandler(page + 1)}
      >
        next
      </button>
    </div>
  );
};
