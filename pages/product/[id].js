import { useRouter } from "next/router";
import React, { useEffect } from "react";

function ProductDetail() {
  const [data, setData] = React.useState([]);
  const router = useRouter();
  const { id } = router.query;
  console.log(id);

  const fetchData = async () => {
    const res = await fetch(`https://dummyjson.com/products/${id}`);
    const data = await res.json();
    console.log(data);
    setData(data);
  };

  useEffect(() => {
    fetchData();
  }, [id]);

  return (
    <div className=" max-w-screen-xl mx-auto p-10 bg-[#181818] rounded-xl text-white m-10">
      <h1 className=" text-3xl">{data.title}</h1>
      <p>{data.description}</p>
      <p>{data.price}</p>

      {data.images &&
        data.images.map((image, i) => {
          return <img key={i} src={image} alt="" />;
        })}
    </div>
  );
}

export default ProductDetail;
